<?php
/**
 * Menu callback; Retrieve a JSON object containing autocomplete suggestions for existing nodes.
 */
function uc_node_panes_autocomplete_product($string) {
  $matches = array();
  $result = db_query("SELECT n.nid, n.title FROM {node} AS n INNER JOIN {uc_products} AS p ON n.vid = p.vid WHERE n.status = 1 AND n.title LIKE '%%%s%%' OR n.nid LIKE '%d%%'", $string, $string);
  while ($node = db_fetch_object($result)) {
    $title = check_plain($node->title);
    $nid = $node->nid;
    $display = $title . ' [' . $nid . ']';
    $matches[$display] = $display;
  }
  drupal_json($matches);
}
<?php
/**
 * Menu callback; Retrieve a JSON object containing autocomplete suggestions for existing nodes.
 */
function uc_node_panes_autocomplete_node($string) {
  $matches = array();
  $result = db_query("SELECT nid, title FROM {node} WHERE status = 1 AND nid LIKE '%s%%' OR title LIKE '%%%s%%'", $string, $string);
  while ($node = db_fetch_object($result)) {
    $title = check_plain($node->title);
    $nid = $node->nid;
    $display = $title . ' [' . $nid . ']';
    $matches[$display] = $display;
  }
  drupal_json($matches);
}